FROM python:3.8

ENV MICRO_SERVICE=/home/app/webapp
RUN mkdir -p $MICRO_SERVICE
WORKDIR $MICRO_SERVICE

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN pip install --upgrade pip

COPY requirements.txt $MICRO_SERVICE
COPY app.py $MICRO_SERVICE
RUN pip install -r requirements.txt
EXPOSE 8501
CMD streamlit run app.py
